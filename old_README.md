# FreeBSD setup on Dell XPS 13 9310

## Install guide
todo

## After first boot (as root)
```bash
pkg install doas bash bash-completion git vim doas
chsh -s /usr/local/bin/bash
echo "permit :wheel" > /usr/local/etc/doas.conf
```
You can change to bash as your regular user as well with the same chsh command in your users shell

# Fix FreeBSD.conf
Add the following to /usr/local/etc/pkg/repos/FreeBSD.conf
```code
FreeBSD: { enabled: no }	# Disables /etc/pkg/FreeBSD.conf "quarterly". 
FreeBSD1: {
	url: "pkg+http://pkg.FreeBSD.org/${ABI}/latest",
	mirror_type: "srv",
	signature_type: "fingerprints",
	fingerprints: "/usr/share/keys/pkg",
	enabled: yes
}
```
Run these commands as root
```bash 
freebsd-update fetch
freebsd-update install
pkg delete drm-kmod
pkg update
pkg upgrade
pkg install drm-kmod
sysrc kld_list="/boot/modules/i915kms.ko"
```

## Install packages (as root)
```bash
pkg install alacritty btop clipmenu curl cwm dmenu evince firacode firefox-esr fzf google-fonts htop neofetch pcmanfm picom wget xorg rofi zsh lynis xlockmore clamav kitty tree ImageMagick7
```

## Add entries to /etc/rc.conf (as root) and reboot
```bash
sysrc dbus_enable=YES
sysrc hald_enable=YES
sysrc apmd_enable=YES
sysrc powerd_enable=YES
sysrc tmpmfs=YES
sysrc tmpsize=512M
sysrc firewall_enable=YES
sysrc firewall_type=closed
sysrc pf_enable=YES

#      Ensure syslogd does not bind to a network socket if you are not logging into a remote machine.
sysrc syslogd_flags="-ss" 

#      ICMP Redirect messages can be used by attackers to redirect traffic and should be ignored.
sysrc icmp_drop_redirect=YES

#      sendmail is an insecure service and should be disabled.
sysrc sendmail_enable=NO

#       The Internet Super Server (inetd) allows a number of simple Internet services to be enabled, including
#       finger, ftp ssh, and telnetd.  Enabling these services may increase risk of security problems by
#       increasing the exposure of your system.
sysrc inetd_enable=NO

#      Network File System allows a system to share directories and files with other computers over a network
#      and should be disabled.
sysrc nfs_server_enable=NO
sysrc nfs_client_enable=NO

#      SSHD is a family of applications that can used with network connectivity tools.
#      This disables rlogin, RSH, RCP and telenet.
sysrc sshd_enable=NO

#      Disable portmap if you are not running Network File Systems.
sysrc portmap_enable=NO

#      Disable computer system details from being added to /etc/motd on system reboot.
sysrc update_motd=NO

#      The /tmp directory should be cleared at startup to ensure that any malicious code that may have
#      entered into the temp file is removed.
sysrc clear_tmp_enable=YES

```

## The sysctl.conf edits
The sysctl.conf file allow you to configure various aspects of a FreeBSD computer. This includes many
advanced options of the TCP/IP stack and virtual memory system that can dramatically improve
performance.

```bash
#      Prevent users from seeing information about processes that are being run under another UID.
echo 'security.bsd.see_other_uids=0' >> /etc/sysctl.conf

#      Generate a random ID for the IP packets as opposed to incrementing them by one.
echo 'net.inet.ip.random_id=1' >> /etc/sysctl.conf

#      This will discover dead connections and clear them.
echo 'net.inet.tcp.always_keepalive=1' >> /etc/sysctl.conf

#      Enabling blackholes for udp and tcp will drop all packets that are received on a closed port and will not
#      give a reply.
echo 'net.inet.tcp.blackhole=2' >> /etc/sysctl.conf
echo 'net.inet.udp.blackhole=1' >> /etc/sysctl.conf

#      Disable ICMP broadcast echo activity.  This could allow the computer to be used as part of a Smurf
#      attack.
sysctl -w net.inet.icmp.bmcastecho=0

#      Disable ICMP routing redirects.  This could allow the computer to have its routing table corrupted by an
#      attacker.
sysctl -w net.inet.ip.redirect=0
sysctl -w net.inet.ip6.redirect=0

#     Disable ICMP broadcast probes.  This could allow an attacker to reverse engineer details of your
#     network infrastructure.
sysctl -w net.inet.icmp.maskrepl=0

#     Disable IP source routing.  This could allow attackers to spoof IP addresses that you normally trust as
#     internal hosts.
sysctl -w net.inet.ip.sourceroute=0
sysctl -w net.inet.ip.accept_sourceroute=0
```

## Disable users from having access to configuration files
Use chmod to disable users access to configuration files
```bash
chmod o= /etc/fstab
chmod o= /etc/ftpusers
chmod o= /etc/group
chmod o= /etc/hosts
chmod o= /etc/hosts.allow
chmod o= /etc/hosts.equiv
chmod o= /etc/hosts.lpd
chmod o= /etc/inetd.conf
chmod o= /etc/login.access
chmod o= /etc/login.conf
chmod o= /etc/newsyslog.conf
chmod o= /etc/rc.conf
chmod o= /etc/ssh/sshd_config
chmod o= /etc/sysctl.conf
chmod o= /etc/syslog.conf
chmod o= /etc/ttys
chmod o= /var/log
```

## Schedule jobs
Enable root as the only account with the ability to schedule jobs
```bash
echo "root" > /var/cron/allow
echo "root" > /var/at/at.allow
chmod o= /etc/crontab
chmod o= /usr/bin/crontab
chmod o= /usr/bin/at
chmod o= /usr/bin/atq
chmod o= /usr/bin/atrm
chmod o= /usr/bin/batch
```

## Secure root account
Secure the root direcotry contents to prevent viewing
```bash
chmod 710 /root
```

## Merge all temporary directories
A single directory should be used for temporary files, not two.
The /var/tmp directory will be replaced with a link to /tmp.
```bash
mv /var/tmp/* /tmp
rm -rf /var/tmp
ln -s /tmp /var/tmp
```

## Secure FreeBSD in single user mode
Edit the /etc/ttys file:
```code
# find this line in the /etc/ttys file
console none	unknown off secure

# change the secure to insecure
console none	unknown off insecure
```
Insecure indicates that the console can be accessed by unauthorized persons, and is 
not secure.

After rebooting and entering single user mode, the user will be prompted for a
password to gain access to the shell prompt.

## Change password encryption
Enable the use of blowfish password encryption for enhanced password security.
```code
# edit /etc/auth.conf
# add the following line 
crypt_default=blf

# edit /etc/login.conf
# change password format from md5 to blf
passwd_format="blf"
```

## Copy bashrc, aliases, xinitrc and cwmrc
```bash
cp bashrc $HOME/.bashrc
cp xinitrc $HOME/.xinitrc
cp cwmrc $HOME/.cwmrc
cp aliases $HOME/.aliases
```

## Copy wallpapers (as your regular user)
```bash
mkdir -p $HOME/.wallpapers
cp unix1920_1047.jpg $HOME/.wallpapers/unix1920_1047.jpg
cp unix.jpg $HOME/.wallpapers/unix.jpg
```

## Copy settings for rofi (as your regular user)
```bash
mkdir -p $HOME/.config/rofi
cp config.rofi $HOME/.config/rofi/config.rofi
```

## Copy rofi scripts (as your regular user)
```bash
mkdir -p $HOME/.config/scripts
cp rofi-randr $HOME/.config/scripts/rofi-randr
chmod +x $HOME/.config/scripts/rofi-randr
```

## Nice list of commands that will help you get along with FreeBSD
```code
https://github.com/hukl/freebsd-toolbox/blob/master/commands.md
```

